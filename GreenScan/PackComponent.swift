//
//  PackCategory.swift
//  GreenScan
//
//  Created by Arsenii on 16/11/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation

class PackComponent {
    
    private(set) var category: String
    private(set) var categoryNumber: String
    private(set) var pack: String
    private(set) var geynCount: Int
    
    private(set) var isWasted: Bool
    
    init(_ category: String, _ categoryNumber: String, _ pack: String, _ geynCount: Int, _ isWasted: Bool) {
        self.category = category
        self.categoryNumber = categoryNumber
        self.pack = pack
        self.geynCount = geynCount
        self.isWasted = isWasted
    }
}
