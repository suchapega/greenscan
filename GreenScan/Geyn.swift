//
//  Geyn.swift
//  GreenScan
//
//  Created by Arsenii on 16/11/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseStorage

//пункты переработки
class Geyn {
        
    private(set) var address: String
    private(set) var position: GeoPoint?
    
    private(set) var categories: [String]?
    
    init(_ address: String, _ position: GeoPoint, _ categories: [String]?) {
              self.address = address
              self.position = position
              self.categories = categories
          }
}
