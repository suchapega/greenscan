//
//  DbManager.swift
//  GreenScan
//
//  Created by Arsenii on 16/11/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import Firebase
import UIKit
import FirebaseStorage

class DbManager {
    
    static let shared = DbManager()
    
    private lazy var db = Firestore.firestore()
    
    func loadGeyn(categoryName: String, categoryNumber: String, completion: @escaping ([Geyn]) -> Void){
        
        let searchingCategory = categoryName + " " + categoryNumber
        
        db.collection("Geyn").getDocuments(){ (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            
            var geyns = [Geyn]()

            for document in querySnapshot!.documents {
                    
                    let tempGeyn = self.geinConvertation(document: document)
                
                    if tempGeyn.categories!.contains(searchingCategory){
                        geyns.append(tempGeyn)
                    }
                }
            completion(geyns)
            }
        }
    }
    
    func loadReceipt(receiptCode: String, completion: @escaping (Reciept?) -> Void) {
        
        db.collection("dim_Receipts").whereField("Receipt_EAN", in: [receiptCode]).getDocuments(){ (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            
            var reciept = [Reciept]()
            
            for document in querySnapshot!.documents {
                    reciept.append(self.receiptConvertation(document: document))
                }
            completion(reciept.first)
            }
        }
        
    }
    
    func loadProduct(productCode: String, completion: @escaping (Product?) -> Void) {

        db.collection("Products").whereField("EAN", in: [productCode]).getDocuments(){ (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            
            var products = [Product]()
            
            for document in querySnapshot!.documents {
                    products.append(self.productConvertation(document: document))
                }
            completion(products.first)
            }
        }
    }
    
    func loadProducts(completion: @escaping ([Product]) -> Void) {
     
        db.collection("Products").getDocuments(){ (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            
            var products = [Product]()
            
            for document in querySnapshot!.documents {
                    products.append(self.productConvertation(document: document))
                }
            completion(products)
            }
        }
    }
    
    private func geinConvertation(document: QueryDocumentSnapshot) -> Geyn {
    
        let tempData = document.data()
        
        let address = tempData["address"] as! String
        let position = tempData["position"] as! GeoPoint
        let categories = tempData["Category"] as! [String]
        
        let tempGyen = Geyn(address,position, categories)
        return tempGyen
    }
    
    private func receiptConvertation(document: QueryDocumentSnapshot) -> Reciept {
    
        let tempData = document.data()
        
        let receiptCode = tempData["Receipt_EAN"] as! String
        let productsCutFB = tempData["Products"] as! [[String: Any]]
        
        var products = [Product]()
        
        for productCutData in productsCutFB {
            
            let eanCode = productCutData["EAN"] as! String
            let name = productCutData["Name"] as! String
            
            products.append(Product(eanCode,name,"",nil))
        }

        let tempReciept = Reciept(receiptCode, products)
        return tempReciept;
    }
    
    private func productConvertation(document: QueryDocumentSnapshot) -> Product {
    
        let tempData = document.data()
        
        let productCode = tempData["EAN"] as! String
        let productName = tempData["Name"] as! String
        let packType = tempData["PackType"] as! String
        
        let productPackComponentsFB = tempData["PackComponents"] as! [[String: Any]]
        let productPackComponents = productPackComponentsFB.map { (componentDic) -> PackComponent in
            
            return PackComponent(componentDic["Category"] as! String,
                          componentDic["CategoryNum"] as! String,
                          componentDic["Pack"] as! String,
                          componentDic["GeynCount"] as! Int,
                          componentDic["isWasted"] as! Bool)
        }
        
        let tempProduct = Product(productCode, productName, packType, productPackComponents)
        return tempProduct;
    }
    
}
