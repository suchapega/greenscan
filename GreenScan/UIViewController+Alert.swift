//
//  UIViewController+Alert.swift
//  GreenScan
//
//  Created by Pavel Chupryna + on 16.11.2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(_ title: String, message: String, actionTitle: String = "OK", action: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { _ in
            action?()
        }))
        present(alertController, animated: true)
    }
}
