//
//  DetectBarcodeType.swift
//  GreenScan
//
//  Created by Pavel Chupryna + on 16.11.2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation

enum BarcodeType {
    case receipt
    case product
}

extension String {
    
    var barcodeType: BarcodeType {
        let isItReceipt = self.count == 29 && self.starts(with: "800")
        return isItReceipt ? .receipt : .product
    }
}
