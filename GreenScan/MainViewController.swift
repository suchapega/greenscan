//
//  MainViewController.swift
//  GreenScan
//
//  Created by Pavel Chupryna + on 15.11.2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import UIKit
import AVFoundation

class MainViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    // MARK: - MainView Controls
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cameraView: CameraView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var scannerAnimationView: UIView!
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addButton.layer.cornerRadius = 8
        historyButton.layer.cornerRadius = 8
        
        setupCameraView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        subscribeOnAppActiveChanges()

        sessionQueue.async {
            self.session.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        unsubscribeOfAppActiveChanges()

        sessionQueue.async {
            self.session.stopRunning()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        cameraView.updateOrientation()
    }
    
    // MARK: - Setup helpers
    
    func setupCameraView() {
        
        session.beginConfiguration()

        if let videoDevice = AVCaptureDevice.default(for: .video) {
            if let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice),
                session.canAddInput(videoDeviceInput) {
                session.addInput(videoDeviceInput)
            }

            let metadataOutput = AVCaptureMetadataOutput()

            if (session.canAddOutput(metadataOutput)) {
                session.addOutput(metadataOutput)

                metadataOutput.metadataObjectTypes = [
                    .itf14,
                    .interleaved2of5,
                    .code39Mod43,
                    .code128,
                    .code39,
                    .code93,
                    .ean13,
                    .ean8,
                    .qr,
                    .upce
                ]

                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            }
        }

        session.commitConfiguration()

        cameraView.layer.session = session
        cameraView.layer.videoGravity = .resizeAspectFill

        cameraView.updateOrientation()
    }
    
    func startScannerAnimation() {
        
        scannerAnimationView.transform = .identity
        UIView.animate(withDuration: 2, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.scannerAnimationView.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
        }, completion: nil)
    }
    
    // MARK: Background / Foreground changes

    private func subscribeOnAppActiveChanges() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(appDidBecomeActive),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }

    private func unsubscribeOfAppActiveChanges() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc private func appDidBecomeActive() {
        startScannerAnimation()
    }
    
    // MARK: - Scanner
    
    var barCodeFounded = false
    
    let session = AVCaptureSession()
    let sessionQueue = DispatchQueue(label: "Session Queue")
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count > 0,
            metadataObjects.first is AVMetadataMachineReadableCodeObject,
            let scan = metadataObjects.first as? AVMetadataMachineReadableCodeObject,
            let scannedBarcode = scan.stringValue {
            
            foundBarCode(barcode: scannedBarcode)
        }
    }
    
    func foundBarCode(barcode: String) {
        
        guard !barCodeFounded else { return }
        barCodeFounded = true
        
        switch barcode.barcodeType {
        case .receipt:
//            let receiptVC = ReceiptViewController()
//            receiptVC.modalPresentationStyle = .pageSheet
//            present(receiptVC, animated: true)
            showAlert("Soon", message: "Scanning receipts will be available later")
            barCodeFounded = false
        case .product:
            loadAndOpenProduct(barcode: barcode)
        }
            
            //get list of products
//            DbManager.shared.loadProducts() { products in
//
//                for product in products {
//                    print(product.name)
//                }
//            }
            //
            
            //get receipt
//            DbManager.shared.loadReceipt(receiptCode: "80082019111610072101710011233") { product in
//                print(product?.eanCode)
//            }
        
            //get geyn
//            DbManager.shared.loadGeyn(categoryName:"PET", categoryNumber: "07") { geins in
//            for gein in geins {
//                print(gein.address)
//                }
//            }
    }
    
    func loadAndOpenProduct(barcode: String) {
        
        DbManager.shared.loadProduct(productCode: barcode) { product in
            
            guard let product = product else {
                self.showAlert("Error", message: "This product is not available in the system")
                self.barCodeFounded = false
                return
            }

            let productVC = ProductViewController(product) {
                self.showMainView()
            }
            productVC.modalPresentationStyle = .overFullScreen
            
            self.hideMainView()
            self.present(productVC, animated: true)
        }
    }
    
    func hideMainView() {
        guard !mainView.isHidden else {
            return
        }
        
        mainView.alpha = 1
        UIView.animate(withDuration: 0.23, animations: {
            self.mainView.alpha = 0
        }) { finished in
            self.mainView.isHidden = finished
        }
    }
    
    func showMainView() {
        guard mainView.isHidden else {
            return
        }
        
        mainView.alpha = 0
        UIView.animate(withDuration: 0.23, animations: {
            self.mainView.alpha = 1
        }) { finished in
            self.mainView.isHidden = !finished
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.barCodeFounded = false
            }
        }
    }
    
    // MARK: - Main Actions
    
    @IBAction func addAction(_ sender: Any) {
        showAlert("Soon", message: "This function will be available later", actionTitle: "Show Demo") {
                    
            let demoBottleProduct = "6415600546348"
//            let demoBoxProduct = "6430015538061"
//            let demoPackProduct = "6430067841157"
            self.loadAndOpenProduct(barcode: demoBottleProduct)
        }
    }
    
    @IBAction func historyMainAction(_ sender: Any) {
        showAlert("Soon", message: "This function will be available later")
    }
}
