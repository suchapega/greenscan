//
//  ComponentView.swift
//  GreenScan
//
//  Created by Pavel Chupryna + on 16.11.2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import UIKit

class ComponentView: UIView {
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryNumberLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var mapCounterLabel: UILabel!
    
    let mapDelegate: (() -> Void)?
    let calendarDelegate: (() -> Void)?
    
    required init?(coder aDecoder: NSCoder) {
        self.mapDelegate = nil
        self.calendarDelegate = nil
        
        super.init(coder: aDecoder)
        initFromNib()
    }
    
    init(_ component: PackComponent, mapDelegate: (() -> Void)?, calendarDelegate: (() -> Void)?) {
        self.mapDelegate = mapDelegate
        self.calendarDelegate = calendarDelegate
        
        super.init(frame: CGRect.zero)
        initFromNib()
        
        titleLabel.text = component.pack
        categoryNumberLabel.text = component.categoryNumber
        categoryLabel.text = component.category
        mapCounterLabel.text = String(component.geynCount)
    }
    
    func initFromNib() {
        Bundle.main.loadNibNamed("ComponentView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.layer.cornerRadius = 12
        mapCounterLabel.layer.cornerRadius = mapCounterLabel.bounds.width / 2
    }
    
    @IBAction func mapAction(_ sender: Any) {
        mapDelegate?()
    }
    
    @IBAction func calendarAction(_ sender: Any) {
        calendarDelegate?()
    }
}
