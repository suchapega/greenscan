//
//  User.swift
//  GreenScan
//
//  Created by Arsenii on 16/11/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage

class User {
   
    //id
    private(set) var guide: String
    
    private(set) var firstName: String
    private(set) var secondName: String
    
    //отсканенные продукты
    private(set) var scanedProducts: [Product]?
    //отсканенные чеки
    private(set) var scanedReciepts: [Reciept]?
    
    init(_ guide: String, _ firstName: String, _ secondName: String, _ scanedProducts: [Product]?, _ scanedReciepts: [Reciept]?) {
           self.guide = guide
           self.firstName = firstName
           self.secondName = secondName
           self.scanedProducts = scanedProducts
           self.scanedReciepts = scanedReciepts
       }
}
