//
//  ProductViewController.swift
//  GreenScan
//
//  Created by Pavel Chupryna + on 16.11.2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import UIKit
import AVFoundation

class ProductViewController: UIViewController {
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var scanOneMoreButton: UIButton!
    @IBOutlet weak var historyButton: UIButton!
    
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var bottleView: UIView!
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var packView: UIView!
    @IBOutlet weak var componentsStackView: UIStackView!
    
    let product: Product!
    let scanOneMoreDelegate: (() -> Void)?
    
    // MARK: - Init
    
    init(_ product: Product, scanOneMoreDelegate: (() -> Void)?) {
        self.product = product
        self.scanOneMoreDelegate = scanOneMoreDelegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        self.product = nil
        self.scanOneMoreDelegate = nil
        super.init(coder: coder)
    }
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scanOneMoreButton.layer.cornerRadius = 8
        historyButton.layer.cornerRadius = 8
        
        productTitle.text = product.name
        
        
        guard let components = product.packComponents else {
            showAlert("Error", message: "This product don't have components for recycling")
            return
        }

        for component in components {
            let componentView = ComponentView(component,
                                              mapDelegate: { self.mapAction(component) },
                                              calendarDelegate: { self.calendarAction(component) })
            componentsStackView.addArrangedSubview(componentView)
            componentView.heightAnchor.constraint(equalToConstant: 170).isActive = true
            componentView.widthAnchor.constraint(equalToConstant: 110).isActive = true
        }
        
        
        guard let packType = product.packType else {
            showAlert("Error", message: "This product type is not available in the system")
            return
        }
        
        switch packType {
        case .bottle:
            bottleView.isHidden = false
        case .box:
            boxView.isHidden = false
        case .pack:
            packView.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        sortIt()
    }
    
    // MARK: - Awesome animation
    
    func sortIt() {
        
        guard let visibleProduct = productView.subviews.first(where: { view in !view.isHidden }) else {
            return
        }
        
        let componentGhost = visibleProduct.subviews.first ?? UIView()
        componentGhost.alpha = 0
        
        let componentsImages = Array(visibleProduct.subviews.dropFirst())
        let componentsItems = componentsStackView.arrangedSubviews
        
        guard componentsImages.count == componentsItems.count else {
            assertionFailure("Component's images and items not equal")
            return
        }
        
        UIView.animate(withDuration: 5, delay: 1, usingSpringWithDamping: 0.75, initialSpringVelocity: 1, options: [.curveEaseInOut], animations: {
            
            for i in 0..<componentsImages.count {
                self.moveViewTo(componentsImages[i], place: componentsItems[i])
            }
            
        }, completion: { finished in
            if finished {
                for componentImage in componentsImages {
                    componentImage.isHidden = true
                }
            }
        })
        
        UIView.animate(withDuration: 0.75, delay: 1, options: [], animations: {
            componentGhost.alpha = 1
        }, completion: nil)
    }
    
    func moveViewTo(_ view: UIView, place: UIView) {
        
        guard let viewTrueCenter = view.superview?.convert(view.center, to: nil) else {
            return
        }
        
        guard let placeTrueCenter = place.superview?.convert(place.center, to: nil) else {
            return
        }
        
        view.transform = CGAffineTransform(scaleX: 0.25, y: 0.25)
                        .concatenating(CGAffineTransform(translationX: placeTrueCenter.x - viewTrueCenter.x, y: placeTrueCenter.y - viewTrueCenter.y))
    }
    
    // MARK: - Component delegates
    
    func mapAction(_ component: PackComponent) {
        showAlert("Soon", message: "This function will be available later")
    }
    
    func calendarAction(_ component: PackComponent) {
        let demoAddress = "Junction 2019"
        let demoTime: Double = 60 * 60 * 42
        RemainderHelper.AddReminder(component.pack, product.name, demoAddress, demoTime) { result in
            DispatchQueue.main.async {
                if result {
                    self.showAlert("Reminder", message: "Recycling task was successfully added")
                } else {
                    self.showAlert("Error", message: "Can't add new reminder task, please check permission settings and try again")
                }
            }
        }
    }
    
    // MARK: - Scan one more
    
    @IBAction func scanOneMoreAction(_ sender: Any) {
        self.scanOneMoreDelegate?()
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Go to history
    
    @IBAction func historyAction(_ sender: Any) {
        showAlert("Soon", message: "This function will be available later")
    }
    
}
