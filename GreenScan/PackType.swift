//
//  PackType.swift
//  GreenScan
//
//  Created by Pavel Chupryna + on 16.11.2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation

enum PackType: String {
    case bottle
    case box
    case pack
}
