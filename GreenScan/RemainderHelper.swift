//
//  RemainderHelper.swift
//  GreenScan
//
//  Created by Arsenii on 17/11/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import EventKit

class RemainderHelper {
    
    static func AddReminder(_ componentName: String, _ productName: String, _ geinAddress: String, _ remaindViaTimeSec: Double, completion: @escaping (Bool) -> Void) {
        
        let eventStore = EKEventStore()
           
        eventStore.requestAccess(to: EKEntityType.reminder, completion: {
         granted, error in
         if (granted) && (error == nil) {
           print("granted \(granted)")

           let reminder:EKReminder = EKReminder(eventStore: eventStore)
           reminder.title = "Recycle component \(componentName) of product \(productName)"
           reminder.priority = 2

           reminder.notes = "Waste recycling address: \(geinAddress)"

           let alarmTime = Date().addingTimeInterval(remaindViaTimeSec)
           let alarm = EKAlarm(absoluteDate: alarmTime)
           reminder.addAlarm(alarm)

           reminder.calendar = eventStore.defaultCalendarForNewReminders()

           do {
             try eventStore.save(reminder, commit: true)
             completion(true)
           } catch {
             print("Cannot save")
             completion(false)
             return
           }
           print("Reminder saved")
         } else {
            completion(false)
            }
        })

        
    }
    
}

