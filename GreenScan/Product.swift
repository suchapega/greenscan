//
//  Product.swift
//  GreenScan
//
//  Created by Arsenii on 16/11/2019.
//  Copyright © 2019 Suchapega. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage

class Product{
    
    //id
    private(set) var eanCode: String
    
    private(set) var name: String
    
    private(set) var packType: PackType?
    
    private(set) var packComponents: [PackComponent]?
    
    init(_ eanCode: String, _ name: String, _ packType: String, _ packComponents: [PackComponent]?) {
        self.eanCode = eanCode
        self.name = name
        self.packType = PackType(rawValue: packType)
        self.packComponents = packComponents
    }
}
