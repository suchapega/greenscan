# GreenScan
![icon](GreenScan/Assets.xcassets/AppIcon.appiconset/Icon-128.png)

Service simplifies the process of daily garbage sorting, helps to choose a place and time for recycling, based on product package components (type of plastic, paper, etc.) and collects personal statistics of recycled products

## How does it works?

1. User scans a receipt or a product barcode
2. Service provides detailed information about product package by categories based on K-Group API data
3. Application allows to add reminders for upcoming waste disposals

##### Prototype
![Prototype with real products](greenscan.gif)

Created by Suchapega